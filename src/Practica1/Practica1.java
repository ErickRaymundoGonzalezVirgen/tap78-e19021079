/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica1;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Erick Gonzalez
 */
public class Practica1 extends JFrame {
   
    
    JLabel lbl;
    JTextField tf;
    JButton btn;
    
    
    public Practica1(){
         this.setSize(400,200);
         this.setLayout(new BorderLayout());
         this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        lbl = new JLabel("Escribe un nombre para saludar");
        tf = new JTextField();
        btn = new JButton("Saluda");
        
        btn.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
                 JOptionPane.showMessageDialog(null, "Hola! "+tf.getText());
             }
         });
        
        this.add(lbl,BorderLayout.LINE_START);
        this.add(tf,BorderLayout.CENTER);
        this.add(btn,BorderLayout.PAGE_END);
        
        
    }    
    
    
    public static void main(String[]args){
        
        Practica1 p1 = new Practica1();
        p1.setVisible(true);
        
        
    }
    
}
