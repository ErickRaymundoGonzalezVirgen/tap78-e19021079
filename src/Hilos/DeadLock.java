/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

/**
 *
 * @author Erick Gonzalez
 */
public class DeadLock {
    
    static class Amigo {
        private final String name;
        public Amigo(String name) {
            this.name = name;
        }
        public String getName() {
            return this.name;
        }
        public synchronized void saludar(Amigo bower) {
            System.out.format("%s: %s"
                + " me ha saludado!%n", 
                this.name, bower.getName());
            bower.devolverSaludo(this);
        }
        public synchronized void devolverSaludo(Amigo bower) {
            System.out.format("%s: %s"
                + " me ha regresado del saludo!%n",
                this.name, bower.getName());
        }
    }

    
    
    public static void main(String[] args) {
        
        final Amigo pancho =
            new Amigo("Pancho");
        
        final Amigo gaston =
            new Amigo("Gaston");
        
        new Thread(() -> {
            pancho.saludar(gaston);
        }).start();
        
        new Thread(() -> {
            gaston.saludar(pancho);
        }).start();
    }
}

