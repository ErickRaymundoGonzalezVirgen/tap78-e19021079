/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erick Gonzalez
 */
public class practica8 {
    
    public static void main(String args[]){
        
        TareaX hilos[] = new TareaX[3];
        
        ThreadGroup tg = new ThreadGroup("Hilos");
       
        hilos[0] = new TareaX("Tarea1",10);
        hilos[1] = new TareaX("Tarea2",5);
        hilos[2] = new TareaX("Tarea3",20);
        
        tg.enumerate(hilos);
        
        tg.setDaemon(true);
        
        tg.interrupt();
        
    }
}

class TareaX extends Thread {

    int i=0;
    String nombre;
    
    public TareaX(String nombre,int i){
        this.i = i;
        this.nombre = nombre;
    }
    
    @Override
    public void run() {
        for (int x = 0; x < this.i; x++){
            System.out.println(this.nombre + "- Hola mundo "+x);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(TareaX.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}