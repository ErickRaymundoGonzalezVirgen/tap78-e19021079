package practicas;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Erick Gonzalez
 */


public class Listener3 extends JFrame {
    JButton jBtn1;
    public Listener3(){
        this.setLayout(new BorderLayout());
        
        jBtn1 = new JButton("Salir");
        jBtn1.addActionListener(
                    new ActionListener(){
                        @Override
                        public void actionPerformed(ActionEvent arg0) {
                            System.exit(0); //To change body of generated methods, choose Tools | Templates.
                        }
                    }
                );
        
        this.add(jBtn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener3 ventana = new Listener3();
        ventana.setSize(300, 200);
        ventana.setVisible(true);
    }
}