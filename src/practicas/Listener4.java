package practicas;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Erick Gonzalez
 */

public class Listener4 extends JFrame {
  JButton jBtn1;
  
  
    public Listener4(){
        
        
       this.setLayout(new BorderLayout());
        
        jBtn1 = new JButton("Salir");
        jBtn1.addMouseListener(
                    new MouseAdapter(){
                        @Override
                        public void mouseClicked(MouseEvent arg0) {
                            System.exit(0);
                        }
                    }
                );
        
        this.add(jBtn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener4 ventana = new Listener4();
        ventana.setSize(300, 200);
        ventana.setVisible(true);
    }  
}