/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Multihilos;


import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erick Gonzalez
 */
public class Cliente {
    
    BufferedReader in;
    PrintWriter    out;
    
    Socket cnx;
    
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    (new Cliente()).start();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    void start() throws InterruptedException {
        String respuesta="", comando = "";
        Conexion hilo;
        Scanner consola = new Scanner(System.in);
        try {
            this.cnx = new Socket("25.60.240.78",4444);
            
            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            out = new PrintWriter(cnx.getOutputStream(),true);
            
            hilo = new Conexion(in);
            hilo.start();//Hilo encargado de lecturas del servidor

            while (!comando.toLowerCase().contains("salir")){   
                 comando = consola.nextLine();
                 out.println(comando);                
            }
            
            hilo.ejecucion = false;
            Thread.sleep(2000);
            
            this.cnx.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    class Conexion extends Thread {
        BufferedReader in;
        public boolean ejecucion = true;
        
        
        public Conexion(BufferedReader in){
            this.in = in;
        }
        
        @Override
        public void run() {
            String respuesta = "";
            while(ejecucion){
                try {            
                    respuesta = in.readLine();
                    if (respuesta!=null) System.out.println(respuesta); 
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        }   
    }
}